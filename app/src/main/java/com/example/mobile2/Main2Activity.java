package com.example.mobile2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        String tmp = getIntent().getStringExtra("value");
        textView=findViewById(R.id.textView);
        textView.setText(tmp);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("valueRet",String.valueOf(textView.getText()));
        setResult(RESULT_OK,intent);
        super.onBackPressed();
    }

    public void add(View view) {
        int tmp = Integer.parseInt(String.valueOf(textView.getText()));
        tmp++;
        textView.setText(Integer.toString(tmp));
    }
}
