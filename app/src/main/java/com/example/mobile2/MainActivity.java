package com.example.mobile2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    EditText search;
    EditText map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText=findViewById(R.id.editText);
        search=findViewById(R.id.search);
        map=findViewById(R.id.map);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(getIntent().getStringExtra("value")!=null){
            editText.setText(getIntent().getStringExtra("value"));
        }
    }

    public void next(View view) {
        Intent intent = new Intent(this,Main2Activity.class);
        String tmp = String.valueOf(editText.getText());
        intent.putExtra("value",tmp);
        startActivityForResult(intent,1);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                editText.setText(data.getStringExtra("valueRet"));
            }
        }
    }

    public void search(View view) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com/#q=" + search.getText())));
    }

    public void mapClick(View view) {
        String tmp = "http://maps.google.co.in/maps?q=" + map.getText();
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(tmp));
        startActivity(i);
    }

    public void gallery(View view) {
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_VIEW);
        intent.setType("image/*");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
